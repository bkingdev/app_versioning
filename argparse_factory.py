'''Model to setup argparse by command'''

import argparse

class ArgParseFactory(object):
    '''Sets up argparse arguments and returns loaded parser when requested'''
    def __init__(self, *args):

        self.pkgs = args
        self.arg_pkgs = []


    def get_loaded_argparser(self):
        '''Loads specified argument packages to argparser'''
        parser = argparse.ArgumentParser()
        if 'AppVersioning' in self.pkgs:
            self.add_appversioning_args(parser)

        return parser

    @staticmethod
    def add_appversioning_args(parser):
        '''Sets up arg line command module'''
        parser.add_argument('-d', '--display', action='store_true',
                            help='Displays versioning info (can filter with '
                            '-cf CUSTOMER -af APPFILTER )')
        parser.add_argument('-cf', '--customerfilter', type=str,
                            help='Filter action to specific customer')
        parser.add_argument('-af', '--appfilter', type=str,
                            help=('Filter action to specific app '
                                  'Note: Cannot be used to apply updates '
                                  'to multiple customers.'))
        parser.add_argument('-v', '--version', type=str,
                            help=('Upserts filtered customer for given -af '
                                  'APPFILTER. Requires -cf CUSTOMER '
                                  '-af APPFILTER) (Requires valid versioning '
                                  'string. Ex: 4.3.1.2)'))
        parser.add_argument('-vgt', '--versiongreaterthan', type=str,
                            help=(('Filters application query to return '
                                   'counties containing the specified app '
                                   'with a version number greather than '
                                   'that specified. (Requires -a APPFILTER '
                                   '-d DISPLAY')))
        parser.add_argument('-vlt', '--versionlessthan', type=str,
                            help=(('Filters application query to return '
                                   'counties containing the specified app '
                                   'with a version number less than '
                                   'that specified. (Requires -a APPFILTER '
                                   '-d DISPLAY')))
        parser.add_argument('-veq', '--versionequalto', type=str,
                            help=(('Filters application query to return '
                                   'counties containing the specified app '
                                   'with a version number equal to '
                                   'that specified. (Requires -a APPFILTER '
                                   '-d DISPPLAY')))
        parser.add_argument('-tz', '--timezone', type=str,
                            help=('Updates filtered customer\'s TimeZone'
                                  '(requires -cf)'))
        parser.add_argument('-n', '--number', type=int,
                            help=('Update filtered customer\'s number '
                                  '(requires -cf)'))
        parser.add_argument('-dbu', '--dbupdate', action='store_true',
                            help=('Update a filtered customers database '
                                  'instance info (requires -cf CUSTOMER)'))
        parser.add_argument('-dbn', '--dbname', type=str,
                            help=(('Specifies dbname. Requires -cf CUSTOMER '
                                  '-dbu FLAG')))
        parser.add_argument('-dbx', '--dbexpress', action='store_true',
                            help=('SQL Express Flag. Requires -dbu '
                                  '-cf CUSTOMER.'))
        parser.add_argument('-dbsp', '--dbservicepack', action='store_true',
                            help=('Specifies service pack for db. Requires '
                                  '-dbu FLAG -cf CUSTOMER'))
        parser.add_argument('-dbv', '--dbversion', type=str,
                            help=('Specifies db version. Requires -dbu FLAG '
                                  '-cf CUSTOMER'))
        parser.add_argument('-cn', '--customername', type=str,
                            help=('Updates a filtered customers name'
                                  '(requires -cf CUSTOMERFILTER)'))
        parser.add_argument('-new', '--new', action='store_true',
                            help=('Creates a new customer.(Requires '
                                  '-cf CUSTOMERFILTER '
                                  '-cn CUSTOMERNAME -tz TIMEZONE -n NUMBER)'))
        parser.add_argument('-rma', '--removeapp', type=str,
                            help=('Removes application from filtered customer'
                                  '(Requires -cn CUSTOMERNAME'))
        return parser
