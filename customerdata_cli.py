'''Main version management CLI Module.
Developed by: Brad King
***Pre-Beta Version
'''


import versioning_dal
import app_config
import cli_orchestrator

CUSTOMER_DAL = versioning_dal.Customers(app_config.Configuration.Test_Mode)

def main():
    '''Main execution routine'''
    orchestrator = cli_orchestrator.Orchestrator(CUSTOMER_DAL)
    orchestrator.orchestrate_updates()
    orchestrator.orchestrate_display()


if __name__ == '__main__':
    main()
