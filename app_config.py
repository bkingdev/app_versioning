'''Customerdata config values'''

class Configuration(object):
    '''namespace for static config vals'''
    Test_Mode = True
    Mongo_Instance_URI = 'mongodb://localhost:27017'
    Versioning_DB = 'App_Versioning'
    Test_Versioning_DB = 'Test_Versioning'
