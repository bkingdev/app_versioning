'''Simple static class to provide console std out with color and/or text decoration'''
import platform
from colorama import Fore, Back, Style

class color(object):

	is_windows = platform.system().upper() == "WINDOWS"
	
	if is_windows == False:
		PURPLE = '\033[95m'
		CYAN = '\033[96m'
		DARKCYAN = '\033[36m'
		BLUE = '\033[94m'
		GREEN = '\033[92m'
		YELLOW = '\033[93m'
		RED = '\033[91m'
		BOLD = '\033[1m'
		UNDERLINE = '\033[4m'
		END = '\033[0m'
	else:
		PURPLE = Fore.PURPLE
		CYAN = Fore.CYAN
		DARKCYAN = Style.DIM + Fore.CYAN
		BLUE = Fore.BLUE
		GREEN = Fore.GREEN
		YELLOW = Fore.YELLOW
		RED = Fore.RED
		BOLD = Style.BRIGHT
		UNDERLINE = ''
		END = Style.RESET_ALL


