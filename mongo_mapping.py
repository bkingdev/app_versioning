'''Library for mapping Mongo objects to ViewModels'''

from structure_core import Application, Customer, Database

class MongoMapper(object):
    '''Primary object for Customer, Database and Application Mappings'''

    def __init__(self):
        pass

    @staticmethod
    def _check_for_value(data, key, default):
        '''Returns default value if value can't be found'''
        if key not in data.keys():
            return default
        else:
            return data[key] if data[key] != None else default

    def map_application(self, data):
        '''Maps a Mongo App Node to ApplicationViewModel'''
        empty_default = ''
        major = self._check_for_value(data, 'Major', empty_default)
        minor = self._check_for_value(data, 'Minor', empty_default)
        hotfix = self._check_for_value(data, 'Hotfix', empty_default)
        build = self._check_for_value(data, 'Build', empty_default)
        name = self._check_for_value(data, 'Name', 'Unknown')
        app = Application(name, major, minor, hotfix, build)
        return app

    def map_database_to_model(self, data):
        '''Maps a Mongo DB Dict to View Model'''
        name = self._check_for_value(data, 'Name', None)
        service_pack = self._check_for_value(data, 'Service_Pack', None)
        version = self._check_for_value(data, 'Version', 'N/A')
        is_express = self._check_for_value(data, 'Is_Express', False)
        database = Database(name, version, is_express, service_pack)
        return database

    def map_customer_to_model(self, data):
        '''Maps a Mongo dict to CustomerViewModel'''
        empty_default = 'N/A'
        name = data['_id']
        c_num = self._check_for_value(data, 'CountyNumber', empty_default)
        t_zone = self._check_for_value(data, 'TimeZone', empty_default)
        customer = Customer(name, c_num, t_zone)

        for app_data in data['Applications']:
            customer.add_application(self.map_application(app_data))

        if 'Database' in data:
            customer.set_database(self.map_database_to_model(data['Database']))

        return customer

    @staticmethod
    def _map_app_to_dict(app):
        '''Maps a single app to mongo dictionary'''
        try:
            app_dict = {}
            app_dict['Name'] = app.name
            app_dict['Major'] = app.major_version
            app_dict['Minor'] = app.minor_version
            app_dict['Hotfix'] = app.hotfix
            app_dict['Build'] = app.build
            return app_dict
        except AttributeError:
            print('Error Parsing Applications with following app:\r')
            print(str(app), '\r\n')

    @staticmethod
    def _map_database_to_dict(datbase):
        '''Maps a single database to mongo dict'''
        try:
            db_dict = {'Name' : datbase.name,
                       'Version' : datbase.version,
                       'Service_Pack' : datbase.service_pack,
                       'Is_Express' : datbase.is_express
                      }
            return db_dict
        except AttributeError:
            print('Error Parsing Applications with following app:\r')
            print(datbase.pretty_print(), '\r\n')

    def map_to_dict(self, customer):
        '''Maps a customer to a mongo dictionary for update / insert'''
        cust_dict = {}
        cust_dict['TimeZone'] = customer.time_zone
        cust_dict['_id'] = customer.name
        cust_dict['CountyNumber'] = customer.number
        cust_dict['Applications'] = []

        try:
            for app in customer.applications:
                app_dict = self._map_app_to_dict(app)
                cust_dict['Applications'].append(app_dict)
        except AttributeError:
            print('{0}-missing applications attribute'.format(customer.name))

        try:
            if customer.database:
                db_dict = self._map_database_to_dict(customer.database)
                cust_dict['Database'] = db_dict
        except AttributeError:
            print('{0}-missing databases attribute.'.format(customer.database))

        return cust_dict
