'''Customer Unit Tests'''
import unittest
import structure_core as core

class TestCustomer(unittest.TestCase):
    '''Tests for Customer Class'''
    def test_customer_saturates(self):
        '''Tests that customer attribs are bindable'''
        customer_name = 'TestCustomer'
        customer_number = 2
        time_zone = 'EST'
        test_customer = core.Customer(customer_name, customer_number,
                                      time_zone)
        self.assertEqual(customer_name, test_customer.name)
        self.assertEqual(customer_number, test_customer.number)

    def test_customer_adds_app(self):
        '''Test that a customer adds an app'''
        test_customer = core.Customer('test', 1, 'EST')
        app = core.Application('TestApp', 1, 0, 0, 0)
        test_customer.add_application(app)
        self.assertEqual(1, len(test_customer.get_applications()))

    def test_customer_has_app(self):
        test_customer = core.Customer('test', 1, 'EST')
        app = core.Application('TestApp', 1, 0, 0, 0)
        test_customer.add_application(app)
        self.assertTrue(test_customer.has_app('TestApp', None))
        self.assertFalse(test_customer.has_app('Blah', None))

    def test_customer_has_app_with_specific_version(self):

        test_customer = core.Customer('test', 1, 'EST')
        app = core.Application('TestApp', 1, 0, 0, 0)
        test_customer.add_application(app)
        self.assertTrue(test_customer.has_app('TestApp', '1.0.0.0'))
        self.assertFalse(test_customer.has_app('TestApp', '2.0.0.0'))

class TestApplication(unittest.TestCase):
    '''Tests for application class'''

    def test_application_staturates(self):
        '''tests that application attribs are bindable'''
        major_version = 1
        minor_version = 0
        hotfix = 0
        build = 0
        expected_print = 'TestApp 1.0.0.0'
        app =core.Application('TestApp', major_version, minor_version,
                hotfix, build)
        self.assertEqual(expected_print, str(app))

    def test_does_throw_valueerror(self):
        '''tests that valueerror is thrown for invalid version'''
        app = core.Application('TestApp', 0, 0, 0, 0)
        self.assertRaises(ValueError, app.update_version, 'blah')

    def test_application_updates(self):
        '''Tests that update_application accurately updates'''
        app = core.Application('TestApp', 0, 0, 0, 0)
        maj, minor, hotfix, build = ('1', '2', '3', '4')
        app.update_version('{0}.{1}.{2}.{3}'.format(maj, minor,
                                                        hotfix, build))
        self.assertEqual(maj, app.major_version)
        self.assertEqual(minor, app.minor_version)
        self.assertEqual(hotfix, app.hotfix)
        self.assertEqual(build, app.build)

    def test_application_v_comparisons(self):
        app1 = core.Application('TestApp', 4, 2, 3, 5)
        app2 = core.Application('TestApp', 4, 2, 3, 6)
        self.assertEqual(False, app1 > app2)
        self.assertEqual(True, app1 < app2)

    def test_raises_version_valueerror(self):
        app1 = core.Application('TestApp', 1, 0, 0, 0)
        app2 = core.Application('Blah', 2, 0, 0, 0)
        self.assertRaises(ValueError, app1.__gt__, app2)

    def test_compare_version_strings(self):
        app1 = core.Application('TestApp', 1, 0, 0, 0)
        self.assertTrue(app1.has_version_greater_than('0.0.0.0'))
        self.assertFalse(app1.has_version_less_than('0.0.0.0'))

class TestDatabase(unittest.TestCase):
    '''Tests database class'''
    def test_database_saturates(self):
        '''Tests that constructor attribs are bindable'''
        db_n = 'CountyRecords'
        db_v = 'SQL2008'
        is_express = True
        service_pack = 'R2'
        target_output = 'CountyRecords SQL2008 Express R2'
        database = core.Database(db_n, db_v, is_express, service_pack)
        self.assertEqual(target_output, str(database))

    def test_db_does_update(self):
        datbase = core.Database('CountyRecords', 'SQL2012', True)
        new_version = 'SQL2008'
        new_is_express = False
        new_service_pack = 'R2'
        new_name = 'CountyRecordsExpress'
        datbase.update(new_name, new_version,
                        new_is_express, new_service_pack)
        self.assertEqual(datbase.name, new_name)
        self.assertEqual(datbase.version, new_version)
        self.assertEqual(datbase.is_express, new_is_express)
        self.assertEqual(datbase.service_pack, new_service_pack)

def print_a_customer():
    '''spits out a dummy customer to command line for aesthetic testing'''

    applications = [
                core.Application('TestApp1', 1, 0, 0, 0),
                core.Application('TestApp2', 2, 1, 5, 6),
                core.CoreApplication('eCCLIX Central')
               ]
    database = core.Database('CountyRecords', 'SQL2012', False)

    customer = core.Customer('Test County', 122, 'EST')
    customer.add_applications(applications)
    customer.set_database(database)

    caution = '*'*10+'FOLLOWING PRINT OF CUSTOMER IS FOR VIEWING ONLY'+'*'*10
    print(caution)
    customer.write_app_status()
    print('*'*len(caution))
    print('\r\n\r\n\r\n')
    print('Tests Begin Now')

if __name__ == '__main__':

    print_a_customer()
    unittest.main()




