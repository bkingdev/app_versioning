'''Module for parsing data for Mongo_Import'''
import structure_core as core

def parse_sql_field_to_db(sql_str):
    '''Parse sql to datamodel'''
    sql_str = sql_str.upper()
    s_pack = 'R2' if 'R2' in sql_str else None
    sql_version = get_sql_version(['2005', '2008', '2012'], sql_str)
    is_express = 'EXPRESS' in sql_str
    datbase = core.Database('CountyRecords', sql_version, is_express, s_pack)
    return [datbase]

def get_int_default_fallback(version_vals, idx, default):
    '''Assigns default value if idx value can't be obtained'''
    if not version_vals:
        return default

    if idx not in range(len(version_vals)):
        return default

    if not version_vals[idx]:
        return default

    val_str = version_vals[idx]
    return int(val_str) if val_str.isdigit() else default

def get_sql_version(years, sql_str):
    '''Gets sql version and year'''
    base_version = 'SQL'

    for year in years:
        if year in sql_str:
            base_version = base_version + year

    return base_version

def split_out_version(version):
    '''
Splits out version string into hierarchy Ex: Major, Minor, Hotfix, Build'''
    default_int = 0
    version_vals = version.split('.')

    if len(version_vals) == 1:
        return {
                'Major' : version,
                'Minor' : 0,
                'Hotfix' : 0,
                'Build' : 0
                }

    v_dict = {}
    v_dict['Major'] = get_int_default_fallback(version_vals, 0, default_int)
    v_dict['Minor'] = get_int_default_fallback(version_vals, 1, default_int)
    v_dict['Hotfix'] = get_int_default_fallback(version_vals, 2, default_int)
    v_dict['Build'] = get_int_default_fallback(version_vals, 3, default_int)

    return v_dict
