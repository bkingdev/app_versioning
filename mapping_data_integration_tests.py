'''MongoMapper Accuracy Tests'''
import unittest
import versioning_dal
import mongo_mapping

class MongoMappingValueTests(unittest.TestCase):
    '''Tests to validate mongo / app_mapping equality'''

    @staticmethod
    def get_target_objects():
        '''Get objects for test'''
        dal = versioning_dal.Customers()
        customer_name = 'ALLEN'
        customer = dal.get_one(customer_name)
        mapper = mongo_mapping.MongoMapper()
        mapped_dic = mapper.map_to_dict(customer)
        mongo_dic = dal.get_raw_document_by_id(customer_name)
        return (mapped_dic, mongo_dic)

    def test_timezones_are_equal(self):
        '''Asserts mapping and mongo have equal tzs'''
        mapped, mon = self.get_target_objects()
        self.assertEqual(mapped['TimeZone'], mon['TimeZone'])

    def test_ids_are_equal(self):
        '''Asserts primary keys are equal'''
        mapped, mon = self.get_target_objects()
        self.assertEqual(mapped['_id'], mon['_id'])

    def test_database_vals_are_equal(self):
        '''Asserts db vals are equal'''
        mapped, mon = self.get_target_objects()
        mon_db = mon['Database']
        mapped_db = mapped['Database']
        for key, val in mapped_db.items():
            mon_val = mon_db[key]
            self.assertEqual(val, mon_val)

    def test_app_vals_are_equal(self):
        '''Asserts app vals are equal'''
        mapped, mon = self.get_target_objects()
        mon_apps = mon['Applications']
        for idx in range(len(mapped['Applications'])):
            for key, val in mapped['Applications'][idx].items():
                mon_val = mon_apps[idx][key]
                self.assertEqual(val, mon_val)

if __name__ == '__main__':
    unittest.main()
