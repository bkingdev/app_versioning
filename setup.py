'''Main mongo import routine'''
from pymongo import MongoClient
from app_config import Configuration
import csv, json
import parsing_functions as import_funcs

def convert_to_json(complete_dict):
    '''Simply dumps dict to json'''
    return json.dumps(complete_dict)


def load_datbases(client, datbase_name, app_list, cnties_dict):
    '''Loads production and test datbase's to live instance'''
    datbase = client[datbase_name]
    datbase.create_collection('Available_Applications')
    avail_apps = datbase['Available_Applications']
    avail_apps.insert_one(app_list)
    datbase.create_collection('County_Applications')
    county_apps = datbase['County_Applications']

    for county_app in cnties_dict:
        county_apps.insert_one(county_app)

def import_to_mongo(app_list, cnties_dict):
    '''Main mongo import routine'''
    user_input = input('''
The following procedure will drop and re-create
Live and Test Versioning Databases.
Are you sure you want to continue?(Y/N)  :''')

    if user_input.upper() != 'Y':
        print('EXITING')
        return

    client = MongoClient(Configuration.Mongo_Instance_URI)

    if Configuration.Versioning_DB in client.database_names():
        client.drop_database(Configuration.Versioning_DB)

    if Configuration.Test_Versioning_DB in client.database_names():
        client.drop_database(Configuration.Test_Versioning_DB)

    load_datbases(client, Configuration.Versioning_DB, app_list, cnties_dict)
    load_datbases(client, Configuration.Test_Versioning_DB,
                    app_list, cnties_dict)

    print('Saved to Mongo.')


def import_routine():
    '''Main import routine'''
    with open('app_versioning.csv', 'r') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rows = list(csv_reader)
        header_row = []
        customer_app_list = []
        header_row = rows[0]

        for row_idx in range(len(rows))[1:]:
            county_dict = {}
            row = rows[row_idx]
            county_dict['_id'] = row[0].upper()
            county_dict['TimeZone'] = row[1]
            county_dict['Applications'] = []

            for col_idx in range(len(header_row))[2:]:
                is_not_sql = header_row[col_idx] != 'SQL'
                is_not_c_number = header_row[col_idx] != 'CountyNumber'

                if is_not_sql and is_not_c_number:

                    if row[col_idx] == '':
                        continue

                    full_version = import_funcs.split_out_version(row[col_idx])
                    county_dict['Applications'].append(
                    {
                        'Name' : header_row[col_idx].upper(),
                        'Major' : full_version['Major'],
                        'Minor' : full_version['Minor'],
                        'Hotfix' : full_version['Hotfix'],
                        'Build' : full_version['Build']
                    })
                elif header_row[col_idx] == 'CountyNumber':
                    county_dict['CountyNumber'] = row[col_idx]
                else:
                    sql_str = row[col_idx]
                    datbase = import_funcs.parse_sql_field_to_db(sql_str)[0]
                    county_dict['Database'] = {
                        'Name' : datbase.name,
                        'Version' : datbase.version,
                        'Is_Express' : datbase.is_express,
                        'Service_Pack' : datbase.service_pack}

            customer_app_list.append(county_dict)

    save_input = input('''
Would you like to save data to new json file for inspection? (Y/N) : ''')

    complete_json = convert_to_json(customer_app_list)

    if save_input.upper() == 'Y':
        with open('apps.json', 'w') as json_file:
            json_file.write(complete_json)

    save_input = input('Would you like to push json to Mongo?(Y/N)  :')
    avail_app_dict = {'_id' : 'SMLLC', 'Available':
                               ['CCLIX', 'DTAX', 'Franchise',
                                'Voter', 'MotorVehicle', 'POSTS',
                                'Payables', 'eCCLIX']
                     }

    if save_input.upper() == 'Y':
        import_to_mongo(avail_app_dict, customer_app_list)

if __name__ == '__main__':

    import_routine()
