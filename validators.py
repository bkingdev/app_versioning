'''Module for model validation'''

def is_valid_version(version_str):
    '''Returns true if version is okay for application version'''
    splits = version_str.split('.')
    return len(splits) == 4
