'''
versioning_dal v1

AppVersioning DataAccessLayer Abstractions via Customers object

Should check configuration for valid Mongo Instance pathing
'''
from pymongo import MongoClient
from app_config import Configuration as AppConfiguration
from mongo_mapping import MongoMapper

class Customers(object):
    '''Primary DAL Object: CRUD Operations for Customer'''

    def __init__(self, testMode=True):
        self.testing_enabled = testMode
        self.mapper = MongoMapper()
        self.client = MongoClient(AppConfiguration.Mongo_Instance_URI)
        live_db = AppConfiguration.Versioning_DB
        test_db = AppConfiguration.Test_Versioning_DB
        db_name = live_db if not testMode else test_db
        self.database = self.client[db_name]

    def get_one(self, county_name):
        '''Return a specific county-application document'''
        c_data = self.database.County_Applications.find_one(county_name)
        if not c_data:
            return None
        model = self.mapper.map_customer_to_model(c_data)
        return model

    def get_all(self):
        '''Return all county-app documents'''
        c_data = self.database.County_Applications.find()

        for dat in c_data:
            yield self.mapper.map_customer_to_model(dat)

    def get_raw_document_by_id(self, mongo_id):
        '''Returns raw document from mongo by id'''
        return self.database.County_Applications.find_one(mongo_id)

    def upsert_customer(self, customer):
        '''Adds a customer if id does not exist else updates customer'''
        dic = self.mapper.map_to_dict(customer)
        self.database.County_Applications.save(dic)
