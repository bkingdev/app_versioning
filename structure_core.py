'''
Core View Models Module -> Applications View Models.
'''
import validators
import parsing_functions
from distutils.version import LooseVersion

class Customer(object):
    '''Cutomer View Model'''
    def __init__(self, name, number, time_zone):
        self.data_initialized = False
        self.name = name
        self.number = number
        self.time_zone = time_zone
        self.applications = []
        self.database = None

    def update_timezone(self, time_zone):
        '''Updates timezone to given value'''
        self.time_zone = time_zone

    def update_number(self, number):
        '''Updates customer number to given value'''
        self.number = number

    def add_application(self, application):
        '''Add an application to the model'''
        self.applications.append(application)

    def add_applications(self, applications):
        '''Add applications to the model'''
        self.applications.extend(applications)

    def get_applications(self):
        '''get application models'''
        return self.applications

    def set_database(self, database):
        '''Add database to the model'''
        self.database = database

    def get_database(self):
        '''Get database model'''
        return self.database

    def has_app(self, app_name, app_version):
        '''
            Returns true if customer has application.
            if app_version is specified, applies version criteria
        '''
        for app in self.applications:
            if app_version is None:
                if app.name == app_name:
                    return True
            else:
                v_split = parsing_functions.split_out_version(app_version)
                return (app.name == app.name and
                        app.major_version == v_split['Major'] and
                        app.minor_version == v_split['Minor'] and
                        app.hotfix == v_split['Hotfix'] and
                        app.build == v_split['Build'])
        return False

    def write_app_status(self, **kwargs):
        '''
           Write Application values. Will only print specific app if app_filter
           is specified. Will return with out printing if app_filter is
           specified and no apps meet criteria
        '''

        app_filt = kwargs['app_filter'] if 'app_filter' in kwargs else None
        apps = self._filter_apps(self.applications, kwargs)
        has_apps = apps is not None and len(apps) > 0
        skip = (has_apps is False or apps is None) and app_filt is not None

        if skip:
            #no apps meet desired criteria. Return without printing
            return

        print('{0}({1}) ->{2} Timezone\r'.format(self.name,
                                                 self.number,
                                                 self.time_zone))

        if self.database is not None:
            print('\tDB Info: ' + str(self.database))

        if has_apps:
            description = 'Applications in use for County'
            self._print_versioned_range(description, apps)

    def _filter_apps(self, apps, kwargs):
        '''Filters apps according to app_filter'''

        app_filt = kwargs['app_filter'] if 'app_filter' in kwargs else None
        gt_filt = kwargs['gt_filter'] if 'gt_filter' in kwargs else None
        lt_filt = kwargs['lt_filter'] if 'lt_filter' in kwargs else None
        eq_filt = kwargs['eq_filter'] if 'eq_filter' in kwargs else None

        if app_filt is not None and len(apps) > 0:
            f_app = next((app for app in apps if app.name == app_filt), None)

            if f_app is None:
                return []

            if self._valid_ver_filter(gt_filt) is True:

                if f_app.has_version_greater_than(gt_filt):
                    pass
                else:
                    return []

            if self._valid_ver_filter(lt_filt) is True:

                if f_app.has_version_less_than(lt_filt):
                    pass
                else:
                    return []

            if self._valid_ver_filter(eq_filt) is True:

                if f_app.has_version_equal_to(eq_filt):
                    pass
                else:
                    return []

            return [f_app] if f_app is not None else []

        else:
            return apps

    @staticmethod
    def _valid_ver_filter(filt):
        '''Returns true is filter is enabled and version filter is valid'''
        return filt is not None and validators.is_valid_version(filt)

    @staticmethod
    def _print_versioned_range(descriptor, obj_coll):
        '''print all applications and databases'''
        for i in range(len(obj_coll)):

            if i == 0:
                print('\t{0}\r'.format(descriptor))

            version = obj_coll[i]

            if not isinstance(version, str):
                version = str(obj_coll[i])

            print('\t\t{0}\r'.format(version))

class CoreApplication(object):
    '''Base Application Behavior'''
    def __init__(self, name):
        self.name = name

    def  __str__(self):
        return self.name

class Application(CoreApplication):
    '''Application Model'''
    def __init__(self, name, major_version, minor_version, hotfix, build):
        super().__init__(name)
        self.major_version = major_version
        self.minor_version = minor_version
        self.hotfix = hotfix
        self.build = build
        self.LooseVersion = LooseVersion(self.raw_version_string())

    def __str__(self):
        '''Returns name and versioned string'''
        return '{0} {1}.{2}.{3}.{4}'.format(self.name, self.major_version,
                                            self.minor_version, self.hotfix,
                                            self.build)

    def raw_version_string(self):
        '''Returns version numbered string'''
        return '{0}.{1}.{2}.{3}'.format(self.major_version, self.minor_version,
                                        self.hotfix, self.build)

    def has_version_greater_than(self, other_version):
        '''Compares raw versioning strings returning true if less than'''
        other_loose = LooseVersion(other_version)
        return self.LooseVersion > other_loose

    def has_version_less_than(self, other_version):
        '''Compares raw versioning strings returning false if less than'''
        other_loose = LooseVersion(other_version)
        return self.LooseVersion < other_loose

    def has_version_equal_to(self, other_version):
        '''Returns true if versions are equal'''
        other_loose = LooseVersion(other_version)
        return self.LooseVersion == other_loose

    def __gt__(self, other):
        '''Returns true if this version is greater than other version'''
        if self.name != other.name:
            raise ValueError('Can\'tcompare applications with different names')

        other_version = LooseVersion(other.raw_version_string())
        return self.LooseVersion > other_version

    def __lt__(self, other):
        '''Returns true if this version is less than other version'''
        if self.name != other.name:
            raise ValueError('Can\'tcompare applications with different names')

        this_version = LooseVersion(self.raw_version_string())
        other_version = LooseVersion(other.raw_version_string())
        return this_version < other_version

    def update_version(self, version_str):
        '''Updates application to given versiong string
        will parse full string to version attributes'''

        if not validators.is_valid_version(version_str):
            raise ValueError('{0} is not a valid version'.format(version_str))

        v_spl = version_str.split('.')
        self.major_version = v_spl[0]
        self.minor_version = v_spl[1]
        self.hotfix = v_spl[2]
        self.build = v_spl[3]

class Database(object):
    '''Primary Database Object'''
    def __init__(self, name, version, is_express, service_pack=None):
        self.name = name
        self.version = version
        self.is_express = is_express
        self.service_pack = service_pack

    def __str__(self):
        description = self.name + ' ' + self.version

        if self.is_express:
            description = description + ' Express'

        if self.service_pack:
            description = description + ' ' + self.service_pack

        return description

    def pretty_print(self):
        '''Pretty prints db data -> Same as (__str__)'''
        return self.__str__(self)

    def update(self, name, version, is_express, service_pack=None):
        '''Updates database attributes to given values'''
        self.name = name
        self.version = version
        self.is_express = is_express
        self.service_pack = service_pack
