'''Testing of Configuration settings in accordance with pymongo API'''

from app_config import Configuration
import unittest
from pymongo import MongoClient, errors

class Mongo_Settings_Test(unittest.TestCase):

	client = MongoClient(Configuration.Mongo_Instance_URI)
	test_collection_name = 'TestCollection'
	test_db_name = 'TestDB'

	def test_does_pymongo_connect(self):
		client = self.client
		db_names = client.database_names()
		assertion_msg = 'No errors raised'
		self.assertEqual(assertion_msg, assertion_msg)

	def test_can_write_to_mongo(self):
		##Consider security concerns and authentication in future
		##Purpose of this test is valid usage of API in coordinance with Config not Security
		client = self.client
		test_db_name = self.test_db_name
		test_collection_name = self.test_collection_name

		db = client[test_db_name]
		
		if test_collection_name not in db.collection_names():
			db.create_collection(test_collection_name)

		self.assertEqual(True, test_collection_name in db.collection_names(), 
			'Failed to find/create collection')

		db.drop_collection(test_collection_name)
		self.assertEqual(False, test_collection_name in db.collection_names(), 
			'Failed to drop ' + test_collection_name)

		self.assertEqual(True, test_db_name in client.database_names(),
			'Failed to find / create ' + test_collection_name)

		client.drop_database(test_db_name)
		self.assertEqual(False, test_db_name in client.database_names(),
			'Failed to drop ' + test_collection_name)
		

if __name__ == '__main__':

	unittest.main()
