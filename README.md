[Presentation Slides](http://www.slideshare.net/bkingsmllc/slideshelf)

#App_Versioning_CLI

On-going development of App_Versioning_CLI, a project which will allow for command line updates of deployed application versions across servers. 

##Command Examples:##

__To display all customer app info__  
customerdata -d

__To filter display of customers by customer__  
customerdata -cf ALLEN -d

__To filter customer application list to specific application__  
customerdata -cf ALLEN -af CCLIX -d

__To get a list of all customers with an app version greater than requested__  
customerdata -af CCLIX -vgt 4.0.0.0

__To get a list of all customers with an app version less than requested__  
customerdata -af CCLIX -vlt 4.0.0.0

__To get a list of all customers in EST Timezone__  
customerdata -tz EST -d

__To filter a list of all customers by database__  
customerdata -dbv SQL2012 -d

__To upsert an application for a given customer and display it back after update__  
customerdata -cf ALLEN -af CCLIX -v 4.2.5.4 -d

__To remove an application from a customer__  
customerdata -rma CCLIX -cf ALLEN

__To add a new customer__ 
customerdata -new -cn BRAD -tz EST -n 122

__To upsert a database for a customer__ 
customerdata -dbu -dbv SQL2012 -dbn CountyRecords -cf ALLEN

