'''Orchestrates and executes command arguments in controlled hierarchy'''
import sys
import structure_core
import parsing_functions as parsers
import validators
import argparse_factory


class Orchestrator(object):
    '''Main orchestration object'''

    def __init__(self, dal):

        arg_factory = argparse_factory.ArgParseFactory('AppVersioning')
        parser = arg_factory.get_loaded_argparser()

        if len(sys.argv) == 1:
            #print help if no args passed and exit
            parser.print_help()
            sys.exit(0)

        self.cli_args = parser.parse_args()
        self.dal = dal
        self.customer_model = self._set_customer_model(self.cli_args, dal)


    @staticmethod
    def _set_customer_model(cli_args, dal):
        '''Sets up customer_model. returns None if None intended'''
        model = None

        if cli_args.display is True and cli_args.customerfilter is None:
            return model

        if cli_args.customerfilter is not None:
            model = dal.get_one(cli_args.customerfilter)

        if model is None and cli_args.customerfilter is not None:
            print('No model found for filtered customer. Exiting...')
            sys.exit(0)
        elif model and cli_args.customerfilter:
            return model

        if cli_args.new is True and cli_args.customername is not None:
            check_model = dal.get_one(cli_args.customername)
            if check_model:
                print('Customer already exists. Exiting...')
                sys.exit(0)
        else:
            pass

        if model is None and cli_args.new is True:
            valid_parms = (cli_args.customername is not None and
                           cli_args.timezone is not None and
                           cli_args.number is not None)
            if valid_parms is False:
                print(('-cn CUSTOMERNAME -t TIMEZONE and -n CUSTOMERNUMBER ',
                       'are required with -n NEW flag'))
                sys.exit(0)
            else:
                model = structure_core.Customer(cli_args.customername,
                                                cli_args.number,
                                                cli_args.timezone)
        elif cli_args.display is True:
            print('Invalid customer for update or display. Exiting')
            sys.exit(0)

        return model

    def orchestrate_updates(self):
        '''Orchestrates data upserts / updates if applicable'''

        comargs = self.cli_args
        if self.customer_model is None:
            return

        if comargs.new is False:
            if comargs.timezone is not None:
                self.customer_model.time_zone = comargs.timezone
            if comargs.number is not None:
                self.customer_model.number = comargs.number
            if comargs.customername is not None:
                self.customer_model.name = comargs.customername
            if comargs.appfilter is not None and comargs.version is not None:
                self._upsert_application(comargs.appfilter, comargs.version)
            if comargs.dbupdate is True:
                self._upsert_database(comargs)
            if comargs.removeapp is True:
                self._remove_application(comargs.removeapp)

        self.dal.upsert_customer(self.customer_model)

    def _remove_application(self, application):
        '''Removes application from model if exists'''
        for idx, obj in enumerate(self.customer_model.applications):
            if obj.name == application:
                self.customer_model.applications.pop(idx)
                break
        else:
            print('Could not find application to remove. Exiting...')
            sys.exit(0)

    def _upsert_application(self, application, version):
        '''Upserts applications attributes with app model info'''
        print('upserting application')
        is_valid = validators.is_valid_version(version)
        if is_valid is False:
            print(('A full versioned string is required to update or '
                   'insert an application. Ex: 1.2.3.4. Exiting...'))
            sys.exit(0)
        else:
            version = parsers.split_out_version(version)
            u_app = structure_core.Application(application,
                                               version['Major'],
                                               version['Minor'],
                                               version['Hotfix'],
                                               version['Build'])
            for idx, obj in enumerate(self.customer_model.applications):
                if obj.name == application:
                    self.customer_model.applications[idx] = u_app
                    break
            else:
                #Adds to list of applications in model
                self.customer_model.add_application(u_app)

    def _upsert_database(self, comargs):
        '''upserts db info'''
        db_model = None
        if self.customer_model.database is not None:
            db_model = self.customer_model.database
            if comargs.dbname is not None:
                db_model.name = comargs.dbname
            if comargs.dbexpress is not None:
                db_model.is_express = comargs.dbexpress is True
            if comargs.dbversion is not None:
                db_model.version = comargs.dbversion
            if comargs.dbservicepack is not None:
                db_model.service_pack = comargs.dbservicepack
        else:
            if comargs.dbversion is None or comargs.dbname is None:
                print('Version and name required to add DB Info. Exiting...')
                sys.exit(0)
            else:
                db_model = structure_core.Database(comargs.dbname,
                                                   comargs.dbversion,
                                                   comargs.dbexpress is True,
                                                   comargs.dbservicepack)
        self.customer_model.set_database(db_model)

    def orchestrate_display(self):
        '''Displays data as requested'''

        if not self.cli_args.display:
            return

        appfilter = self.cli_args.appfilter
        vgtfilter = self.cli_args.versiongreaterthan
        vltfilter = self.cli_args.versionlessthan
        veqfilter = self.cli_args.versionequalto
        dbfilter = self.cli_args.dbversion
        tzfilter = self.cli_args.timezone
        customers = []

        if self.customer_model is not None:
            customers.append(self.customer_model)
        else:
            customers = self.dal.get_all()

        for cust in customers:
            skip = self._skip_cust(cust, tzfilter, dbfilter)

            if skip is True:
                continue

            cust.write_app_status(app_filter=appfilter, gt_filter=vgtfilter,
                                  lt_filter=vltfilter, eq_filter=veqfilter)

    @staticmethod
    def _skip_cust(custmodel, tzfilter, dbfilter):
        '''Returns true if model passes filters'''

        filter_tz = tzfilter is not None
        filter_db = dbfilter is not None

        if filter_tz is True:
            if custmodel.time_zone == tzfilter:
                pass
            else:
                return True
        else:
            pass

        if filter_db is True:
            if custmodel.database.version == dbfilter:
                pass
            else:
                return True
        else:
            pass

        return False

